# Visvalingam’s Line Simplification Algorithm
> My implementations of Visvalingam's line simplification algorithm in JS and C&sharp;.

There's a [demo](https://starbeamrainbowlabs.com/labs/line-simplification/demo.html) ([development demo](https://sbrl.gitlab.io/line-simplification/)) available if you'd like to try it out before installing it and using it in your project :smiley_cat:

## Install
Installing this npm package is easy. Make sure you've got `npm`, and then simply run:

```bash
npm install --save visvalingam-simplifier
```

## Usage
This package is written as an ES6 module, so it should work on any platform that supports it. Make sure you have the npm package installed (see above). Then you can use it like so:

```js
import { simplify_line } from "visvalingam-simplifier";

let points_simple = simplify_line([
    { x:389, y: 361 },
    { x:445, y: 239 },
    { x:461, y: 228 },
    { x:501, y: 208 },
    { x:556, y: 195 },
    { x:603, y: 209 },
    { x:678, y: 262 },
    { x:735, y: 279 },
    { x:795, y: 263 },
    { x:838, y: 222 },
    { x:861, y: 172 }
], 6);
```

The above will work if you're using a preprocessor such as [browserify](http://browserify.org/), [webpack](https://webpack.js.org/), or [babel](https://babeljs.io/) - but it might not work so well if you're using pure Node.JS or in the browser without any of the aforementioned preprocessors. In those cases, you may need to tweak the import statement a bit:

For Node.js:

```js
const simplify_line = require("visvalingam-simplifier");
```

For the browser:

```js
import { simplify_line } from "./node_modules/visvalingam-simplifier/simplify_line.js";
```

If you encounter any issues (especially relating to importing / requiring it) - please do let me know by [opening an issue](https://gitlab.com/sbrl/line-simplification/issues/new)! I'll be happy to help :smiley_cat:

### C&sharp; Version
I've also ported the algorithm to C&sharp; for a project of mine. You can find it here: [SimplifyLine.cs](https://gitlab.com/sbrl/line-simplification/blob/master/SimplifyLine.cs). It's actually a slightly modified copy of the real implementation I've done for a secret project of mine, so to that end you'll have to implement your own `Vector` class - or tweak it to use the [Vector2 class](https://gist.github.com/sbrl/589aac9044145cc41a008ca474758c7e) I wrote a while ago.

## Contribute
Found a bug? Experiencing difficulties? Please [open an issue](https://gitlab.com/sbrl/line-simplification/issues/new)! Pull requests are accepted too - so if you've got an improvement / bug fix, I'll glading merge :smiley_cat:

## Links
I found the follwing links useful when implementing this module:

 - https://bost.ocks.org/mike/simplify/

## License
This package is licensed under the _Mozilla Public License 2.0_. The full license text is available [here](https://gitlab.com/sbrl/line-simplification/blob/master/LICENSE) - along with a summary by GitHub on what you can and can't so with it.

If you'd like to do something that's prohibited by the license - please do get in touch! My email address / contact details are available on my [GitLab](https://gitlab.com/users/sbrl/) or [GitHub](https://github.com/sbrl) profiles.

