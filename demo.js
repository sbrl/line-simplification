"use strict";

import Vector from './lib/Vector.js';
import Mouse from './lib/Mouse.js';
import Keyboard from './lib/Keyboard.js';

import { simplify_line } from './simplify_line.js';


class RDPDemo
{
	constructor()
	{
		this.canvas = document.getElementById("canvas-main");
		this.context = this.canvas.getContext("2d");
		
		this.keyboard = new Keyboard();
		this.mouse = new Mouse();
		
		this.rawLine = {
			width: 2,
			colour: "hsl(232, 55%, 58%)",
			
			crossSize: 3,
			crossLineWidth: 1,
			crossColour: "hsl(240, 54%, 44%)"
		};
		this.simpleLine = {
			width: 2,
			colour: "hsl(107, 56%, 53%)",
			
			crossSize: 3,
			crossLineWidth: 1,
			crossColour: "hsl(109, 86%, 14%)"
		};
		
		this.drawRawLine = true;
		this.drawSimpleLine = true;
		
		// ---------------------------
		
		this.canvas.addEventListener("mousedown", this.handleMouseDown.bind(this));
		this.canvas.addEventListener("mousemove", this.handleMouseMove.bind(this));
		
		this.keyboard.on("keyup-1", ((event) => this.drawRawLine = !this.drawRawLine).bind(this));
		this.keyboard.on("keyup-2", ((event) => this.drawSimpleLine = !this.drawSimpleLine).bind(this));
		
		// ---------------------------
		
		this.points = [];
		this.simplePoints = [];
		
		this.epsilon = 2;
		
		this.matchWindowSize();
	}
	
	trackWindowSize()
	{
		document.addEventListener("resize", this.matchWindowSize.bind(this));
	}
	
	matchWindowSize()
	{
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
	}
	
	handleMouseDown(event)
	{
		this.points.length = 0;
	}
	
	handleMouseMove(event)
	{
		if(!this.mouse.leftDown)
			return;
		
		this.points.push(new Vector(event.clientX, event.clientY));
	}
	
	nextFrame()
	{
		this.update();
		this.render(this.canvas, this.context);
		
		requestAnimationFrame(this.nextFrame.bind(this));
	}
	
	update()
	{
		// Update settings
		this.epsilon = parseFloat(document.getElementById("input-epsilon").value);
		
		// Update the interface
		document.getElementById("output-point-count").innerHTML = this.points.length;
		document.getElementById("output-simple-point-count").innerHTML = this.simplePoints.length;
		// todo Change this to be an event listener
		document.getElementById("output-epsilon").innerHTML = parseFloat(this.epsilon).toFixed(2);
		
		// Update canvas
		this.simplePoints = simplify_line(this.points, this.epsilon)
	}
	
	render(canvas, context)
	{
		if(this.points.length == 0)
			return;
		
		// Clear the screen
		canvas.width = canvas.width;
		
		if(this.drawRawLine)
			this.draw_line(canvas, context, this.points, this.rawLine);
		if(this.drawSimpleLine)
			this.draw_line(canvas, context, this.simplePoints, this.simpleLine)
	}
	
	draw_line(canvas, context, points, settings) {
		// Draw the points
		context.beginPath();
		context.moveTo(points[0], points[0]);
		for (let point of points) {
			context.lineTo(point.x, point.y);
		}
		
		context.lineWidth = settings.width;
		context.strokeStyle = settings.colour;
		context.stroke();
		
		// Draw crosses at all the points
		context.beginPath();
		for (let point of points) {
			context.moveTo(point.x - settings.crossSize, point.y - settings.crossSize);
			context.lineTo(point.x + settings.crossSize, point.y + settings.crossSize);
			
			context.moveTo(point.x + settings.crossSize, point.y - settings.crossSize);
			context.lineTo(point.x - settings.crossSize, point.y + settings.crossSize);
		}
		
		context.lineWidth = settings.crossLineWidth;
		context.strokeStyle = settings.crossColour;
		context.stroke();
	}
}


window.addEventListener("load", function(event) {
	document.getElementById("message-es6-modules").style.display = "none";
	
	this.rdpDemo = new RDPDemo();
	this.rdpDemo.nextFrame();
});
